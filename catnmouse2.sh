#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}



THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

while
   echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE"
   echo "Make a guess:"
   read aGuess

   if [ $aGuess -lt $THE_NUMBER_IM_THINKING_OF ]
   then 
      echo "No cat ... the number I'm thinking of is larger than $aGuess"
   fi
   if [ $aGuess -gt $THE_NUMBER_IM_THINKING_OF ]
   then 
      echo "No cat ... the number I'm thinking of is smaller than $aGuess"
   fi
   if [ $aGuess -eq $THE_NUMBER_IM_THINKING_OF ] 
   then
      echo "You got me."
      echo " /|_/|"
      echo "( o.o )"
      echo " > ^ <"
   fi

 [ $aGuess -lt 1 ] || [ $aGuess -gt $THE_MAX_VALUE ] || [ $aGuess -ne $THE_NUMBER_IM_THINKING_OF ]
do
   if [ $aGuess -lt 1 ] || [ $aGuess -gt $THE_MAX_VALUE ]
   then
      echo "error: please enter a number from 1 to $THE_MAX_VALUE"
   fi
done
echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

